﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GUIHolder : MonoBehaviour
{
	public static GUIHolder Instance; // static instance so i can access functions from other classes


	public GameObject currentWeapon;
	BaseWeapon CurrentGun;

	

    public TextMeshProUGUI ammoOnCurrentMagText;
    public TextMeshProUGUI totalAmmoText;
    public TextMeshProUGUI gunNameText;
    public TextMeshProUGUI gunCaliberText;
    public RawImage crosshair;

    void Awake()
	{
		Instance = this;		
	}

	void Start()
    {
		UpdateUI();
	}

	void LateUpdate() // Runs after Update()
	{
		UpdateUI();
	}

	void UpdateUI()
	{
        gunNameText.text = CurrentGun.weaponName;
        gunCaliberText.text = CurrentGun.weaponCaliber;
		totalAmmoText.text = CurrentGun.totalAmmo.ToString();
		ammoOnCurrentMagText.text = CurrentGun.ammoOnCurrentMag.ToString();
	}

	public void SetGun(GameObject _weapon, BaseWeapon _base) //Func so we can change guns on UI
	{
		currentWeapon = _weapon;
		CurrentGun = _base;
	}

    void UpdateCrosshair()
    {

    }
}
