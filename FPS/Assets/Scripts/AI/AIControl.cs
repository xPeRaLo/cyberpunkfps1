﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Unity.Jobs;
using Unity.Collections;


public enum AIState { Searching, Attacking, Chasing } // Enums for handling states

[RequireComponent(typeof(NavMeshAgent))]
public class AIControl : MonoBehaviour
{

	
	NavMeshAgent agent;

	public Transform target; // AI's target

	private AIState state; // State machine


	[Header("AI Stats")]
	public float AIDistanceTravel;



	private int currentHP;


	void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
		
	}

	void Start() 
	{
		state = AIState.Searching;
	}


	// Update is called once per frame
	void FixedUpdate()
	{
		AIBehaviour();
	}

	private void LateUpdate()
	{

	}


	void AIBehaviour()
	{

		Vector3 distanceToPlayer = target.position - this.transform.position;
		float angleToPlayer = Vector3.Angle(distanceToPlayer, this.transform.forward);

		//PLACEHOLDER VARS TO GET RID OF ERRORS, WILL BE OVERRIDEN FOR ACTUAL VALUES OF SCRIPTABLE OBJECTS WITH ENEMY STATS

		float placeHolderDistance = 20f;
		float placeHolderRadius = 90f;
		float placeHolderDistanceToAttack = 10f;

		#region AI State Machine

		switch (state)
		{

			case AIState.Searching: // Generating position for AI


				if (Vector3.Distance(target.position, this.transform.position) < placeHolderDistance &&
					angleToPlayer < placeHolderRadius)
				{ // If distance to player is < x and field of view of enemy as well

					state = AIState.Chasing; // Set State to chasing enemy
					break;
				}

				Vector3 randPos = SetAIPath(transform.position, 30, -1);
				agent.SetDestination(randPos);

				break;

			case AIState.Chasing: // Raching position state for AI


				if (Vector3.Distance(target.position, this.transform.position) > placeHolderDistance &&
					angleToPlayer > placeHolderRadius)
				{ // If distance to player is < x and field of view of enemy as well

					state = AIState.Searching; // Set State to chasing enemy
					break;
				}

				if (Vector3.Distance(target.position, this.transform.position) < placeHolderDistanceToAttack) // If distance between AI and player is > X and field of view
				{
					state = AIState.Attacking;
					break;
				}

				agent.SetDestination(target.position);

				break;
				

			case AIState.Attacking: // Attack state for AI

				if (Vector3.Distance(target.position, this.transform.position) > placeHolderDistanceToAttack) // If distance between AI and player is > X and field of view
				{
					agent.ResetPath(); // Clears Path
					state = AIState.Chasing; // Sets state for searching
					break;
				}

				// Attack
				break;
		}

		#endregion

		Vector3 SetAIPath(Vector3 origin, float dist, int layermask)
		{

			Vector3 randDirection = Random.insideUnitSphere * dist;

			randDirection += origin;

			NavMeshHit navHit;

			NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

			return navHit.position;
		}
	}
}