﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class QuestGiver : MonoBehaviour
{
    public Quest quest;

    public PlayerRef player;

    public GameObject questWindow;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI rewardText;

    public void OpenQuestWindow()
    {
        questWindow.SetActive(true);
        titleText.text = quest.title;
        descriptionText.text = quest.description;
        rewardText.text = quest.reward;
    }
}
