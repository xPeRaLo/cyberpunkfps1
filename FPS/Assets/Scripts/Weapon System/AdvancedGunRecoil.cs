﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedGunRecoil : MonoBehaviour
{
    [Header("Reference Points")]
    public Transform recoilPosition;
    public Transform rotationPoint;
    [Space(10)]

    [Header("Speed Settings")]
    public float positionalRecoilSpeed = 5f;
    public float rotationalRecoilSpeed = 8f;
    public float positionalReturnSpeed = 10f;
    public float rotationalReturnSpeed = 36f;
    [Space(10)]

    //[Header("Amount Settings")]
    //public Vector3 recoilRotation = new Vector3(10, 5, 7);
    //public Vector3 recoilKickBack = new Vector3(0.015f, 0f, -0.2f);
    //public Vector3 recoilRotationAimed = new Vector3(10, 4, 6);
    //public Vector3 recoilKickBackAimed = new Vector3(0.014f, 0f, -0.17f);

    Vector3 rotationalRecoil;
    Vector3 positionalRecoil;
    Vector3 rot;

    [Header("State")]
    public bool aiming;

    private void FixedUpdate()
    {
        rotationalRecoil = Vector3.Lerp(rotationalRecoil, Vector3.zero, rotationalReturnSpeed * Time.deltaTime);
        positionalRecoil = Vector3.Slerp(positionalRecoil, Vector3.zero, positionalRecoilSpeed * Time.deltaTime);

        recoilPosition.localPosition = Vector3.Slerp(recoilPosition.localPosition, positionalRecoil, positionalRecoilSpeed * Time.fixedDeltaTime);
        rot = Vector3.Slerp(rot, rotationalRecoil, rotationalRecoilSpeed * Time.fixedDeltaTime);
        rotationPoint.localRotation = Quaternion.Euler(rot);       
    }

    private void Update()
    {
        if (Input.GetButton("Fire2"))
        {
            aiming = true;
        }
        else
        {
            aiming = false;
        }
    }

    public void Recoil(Vector3 recoilRotationAimed, Vector3 recoilKickBackAimed, Vector3 recoilRotation, Vector3 recoilKickBack)
    {
        if (aiming)
        {
			positionalRecoil += new Vector3(Random.Range(-recoilKickBackAimed.x, recoilKickBackAimed.x) * Time.fixedDeltaTime, Random.Range(-recoilKickBackAimed.y, 0) * Time.fixedDeltaTime, Random.Range(-recoilKickBackAimed.z, recoilKickBackAimed.z) * Time.fixedDeltaTime);
			rotationalRecoil += new Vector3(-recoilRotationAimed.x, Random.Range(-recoilRotationAimed.y, recoilKickBackAimed.y) * Time.deltaTime, Random.Range(-recoilRotationAimed.z, recoilRotationAimed.z) * Time.deltaTime);
        }
        else
        {
			positionalRecoil += new Vector3(Random.Range(-recoilKickBack.x, recoilKickBack.x) * Time.fixedDeltaTime, Random.Range(-recoilKickBack.y, 0), Random.Range(-recoilKickBack.z, recoilKickBack.z) * Time.fixedDeltaTime);
            rotationalRecoil += new Vector3(-recoilRotation.x, Random.Range(-recoilRotation.y, recoilRotation.y) * Time.fixedDeltaTime, Random.Range(-recoilRotation.z, recoilRotation.z) * Time.fixedDeltaTime);
            
        }
    }
}
