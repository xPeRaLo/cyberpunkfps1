﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedCamRecoil : MonoBehaviour
{
    [Header("Camera Recoil Settings")]
    public float rotationSpeed = 6f;
    public float returnSpeed = 25f;
    [Space()]

    [Header("Camera State")]
    public bool aiming;

    private Vector3 currentRotation;
    private Vector3 rot;

    private void FixedUpdate()
    {
        currentRotation = Vector3.Lerp(currentRotation, Vector3.zero, returnSpeed * Time.fixedDeltaTime);
        rot = Vector3.Slerp(rot, currentRotation, rotationSpeed * Time.fixedDeltaTime);
        transform.localRotation = Quaternion.Euler(rot);
    }

    public void Recoil(Vector3 recoil, Vector3 aimedRecoil) // Recoil function
    {
        if (aiming) 
        {
			currentRotation += new Vector3(Random.Range(0, aimedRecoil.x), Random.Range(-aimedRecoil.y, aimedRecoil.y), Random.Range(-aimedRecoil.z, aimedRecoil.z));          
        }
        else
        {
			currentRotation += new Vector3(Random.Range(0, recoil.x), Random.Range(-recoil.y, recoil.y), Random.Range(-recoil.z, recoil.z));
		}
    }

    private void Update()
    {
        if (Input.GetButton("Fire2"))
        {
            aiming = true;
        }
        else
        {
            aiming = false;
        }
    }
}
