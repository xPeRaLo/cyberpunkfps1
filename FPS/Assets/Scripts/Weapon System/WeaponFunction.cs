﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class WeaponFunction : MonoBehaviour {


	[SerializeField] BaseWeapon gun;
	[SerializeField] AdvancedGunRecoil gunRecoilHolder;
	[SerializeField] WeaponSway gunSway; // reference to get Weapon Sway
	[SerializeField] PlayerMove player;

    AdvancedCamRecoil recoilHolder;
    AudioSource weaponAudio;
    private Vector3 originalPosition;

    //aimFOV private References
    private float startFOV;

    public void Awake()
	{
		recoilHolder = GameObject.Find("RecoilHolder").GetComponent<AdvancedCamRecoil>(); // Grab reference to recoilHolder, If recoilHolder were to change change here as well.
	}

	void Start()
	{
		weaponAudio = GetComponent<AudioSource>();
        originalPosition = transform.localPosition;
        startFOV = player.myCamera.fieldOfView;
		
		GUIHolder.Instance.SetGun(this.gameObject, gun); // Call the setGun Function on the GUIHolder instance
	}

	void OnEnable() //Everytime a GameObject activates
	{
		gun.isReloading = false;
		gun.canShoot = true;
		gun.isHoldingGun = true;

		GUIHolder.Instance.SetGun(this.gameObject, gun); // Call the setGun Function on the GUIHolder instance
	}
	void OnDisable()
	{
		gun.isReloading = false;
		gun.canShoot = false;
		gun.isHoldingGun = false;
	}

	void Update()
	{

		if(gun == null) // if no gun
		{
			Debug.Log("No Scriptable object Assigned");
			return;
		}

		if(gun.ammoOnCurrentMag <= 0 && !gun.isReloading && gun.totalAmmo > 0 && gun.isHoldingGun)  // if there is ammo on reserve, but current mag is 0 and isnt reloading
		{
			StartCoroutine(gun.Reload());
		}

		if (gun.isHoldingGun && gun != null) //If he's holding gun and exists
		{
			WeaponHandle(gun);
            AimDownSights(gun);
            CheckSway();
		}
	}

	#region Functions/ Methods/ Coroutines
	void WeaponHandle(BaseWeapon _weapon) //Handles weapon behaviour
	{

		if (_weapon.isHoldingGun)
		{
			if (_weapon.canShoot)
			{
				CheckAmmo(_weapon);
			}

			// Checks if gun is auto or not to determine if you can hold button to fire
			switch (_weapon.weaponType)
			{

				case WeaponType.AUTOMATIC:

					if (Input.GetMouseButton(0) && _weapon.canShoot && !_weapon.isReloading)
					{
						Shoot(_weapon);
					}

					break;

				case WeaponType.SEMIAUTOMATIC:

					if (Input.GetMouseButtonDown(0) && _weapon.canShoot && !_weapon.isReloading)
					{
						Shoot(_weapon);
					}
					break;
			}
		}
	}

	void CheckAmmo(BaseWeapon _weapon)
	{
		if (_weapon.isHoldingGun)
		{
			if (_weapon.ammoOnCurrentMag <= 0 && !_weapon.isReloading)
			{
				if (_weapon.ammoOnCurrentMag <= 0 && _weapon.totalAmmo > 0)
				{
					//Start reload Coroutine
					StartCoroutine(_weapon.Reload());
				}			
			}

            if (_weapon.ammoOnCurrentMag > 0 && !_weapon.isReloading && Input.GetKeyDown(_weapon.reloadKey) && _weapon.ammoOnCurrentMag < _weapon.magSize)
            {
                StartCoroutine(_weapon.Reload());
            }
        }
	}
		

	void Shoot(BaseWeapon _weapon) // Shoot behaviour, takes a gun SO and a muzzleEnd gameobject (to get muzzle end position) as parameters
	{

		if (_weapon.isHoldingGun) 
		{
			if (_weapon.canShoot && _weapon.ammoOnCurrentMag > 0)
			{
				//Shoot coroutine
				StartCoroutine(_weapon.Shoot());
				recoilHolder.Recoil(_weapon.cameraRecoil, _weapon.cameraAimedRecoil); // Call recoil function passing both recoils
                gunRecoilHolder.Recoil(_weapon.recoilRotationAimed, _weapon.recoilKickBackAimed, _weapon.recoilRotation, _weapon.recoilKickBack); //
				_weapon.PlayShootingSFX(weaponAudio);
				//_weapon.PlayMuzzleFlash(_weapon.muzzleFlashFX, muzzleEnd);
				_weapon.RaycastLogic(_weapon);
                
			}
			else if (_weapon.ammoOnCurrentMag <= 0 && _weapon.totalAmmo <= 0)
			{
				//Play empty mag SFX
				Debug.Log("No Ammo");
			}

		}
	}

    void AimDownSights(BaseWeapon _weapon)
    {

        if (Input.GetButton("Fire2") && !gun.isReloading) 
        {
			_weapon.isAiming = true;

            transform.localPosition = Vector3.Lerp(transform.localPosition, gun.aimPosition, Time.deltaTime * gun.aimDownSightSpeed);
            if (player.myCamera.fieldOfView > startFOV - gun.aimFOVChange)
            {
                player.myCamera.fieldOfView = Mathf.Lerp(player.myCamera.fieldOfView, startFOV - gun.aimFOVChange, gun.aimFOVLerpTime * Time.deltaTime);
            }
        }
        else
        {
			_weapon.isAiming = false;

			transform.localPosition = Vector3.Lerp(transform.localPosition, originalPosition, Time.deltaTime * gun.aimDownSightSpeed); 
            if (player.myCamera.fieldOfView < startFOV)
            {
                player.myCamera.fieldOfView = Mathf.Lerp(player.myCamera.fieldOfView, startFOV, gun.aimFOVLerpTime * Time.deltaTime);
            }
        }
    }

    void CheckSway()
    {
        if (Input.GetButton("Fire2") && !gun.isReloading)
        {
            gunSway.Sway(gun.aimedSwayAmount, gun.aimedSmoothAmount, gun.aimedMaxSwayAmount); // values tweakable in the scriptable object
        }
        else
        {
            gunSway.Sway(gun.swayAmount, gun.smoothAmount, gun.maxSwayAmount); // values tweakable in the scriptable object
        }
    }

    #endregion
}

