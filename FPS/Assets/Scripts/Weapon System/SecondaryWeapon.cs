﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "New Secondary.", menuName = "Weapon/Secondary")]
public class SecondaryWeapon : BaseWeapon
{

	void OnEnable()
	{
		isHoldingGun = true;
		
	}

	void OnDisable()
	{
		isHoldingGun = false;
	}


	// Implement Full Auto gun behaviour here
	public override IEnumerator Shoot()
	{

		if (isReloading || !canShoot)
		{
			yield break;
		}

		canShoot = false;
		ammoOnCurrentMag--;
		yield return new WaitForSecondsRealtime(waitPerShot);
		canShoot = true;
	}
}
