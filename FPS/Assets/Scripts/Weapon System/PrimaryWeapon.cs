﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(fileName = "New Primary.", menuName = "Weapon/Primary")]
public class PrimaryWeapon : BaseWeapon
{
    

	void OnEnable()
	{
		isHoldingGun = true;
	}

	void OnDisable()
	{
		isHoldingGun = false;
	}


	// Implement Full Auto gun behaviour here
	public override IEnumerator Shoot()
	{

		if (isReloading || !canShoot)
		{
			yield break;
		}
       
		canShoot = false;
		ammoOnCurrentMag--;
		yield return new WaitForSecondsRealtime(waitPerShot);
		canShoot = true;
	}

	

    
}
