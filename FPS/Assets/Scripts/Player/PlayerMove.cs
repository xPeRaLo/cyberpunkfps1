﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [Header("Input Name")]
    [SerializeField] private string horizontalInputName;
    [SerializeField] private string verticalInputName;
    [Space()]

    [Header("Character Speed")]
    [SerializeField] private float walkSpeed, runSpeed;
    [SerializeField] private float runBuildUpSpeed;
    [SerializeField] private KeyCode runKey;
    [SerializeField] private float ADSWalkSlow, ADSRunSlow;
    [SerializeField] private float slowDownMultiplier;
    [Space()]

    [Header("FOV Values")]
    [SerializeField] private float sprintFOVChange = 5f; //todo: tweak this
    [SerializeField] private float sprintFOVLerpTime = 0.04f; //todo: tweak this
    [Space()]

    [Header("Jump Values")]
    [SerializeField] private float slopeForce;
    [SerializeField] private float slopeForceRayLength;
    [SerializeField] private AnimationCurve jumpFallOff;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private KeyCode jumpKey;
    [Space()]

    [Header("Camera")]
    public Camera myCamera;
    [Space()]

    private bool isJumping;
    private float startFOV;
    private float movementSpeed;
    private CharacterController charController;
    public BaseWeapon gun;
    float originalWalkSpeed;
    float originalRunSpeed;

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        startFOV = myCamera.fieldOfView;
        originalRunSpeed = runSpeed;
        originalWalkSpeed = walkSpeed;
    }

    private void FixedUpdate()
    {
        PlayerMovement();       
    }

    private void PlayerMovement()
    {
        float vertInput = Input.GetAxis(verticalInputName);
        float horInput = Input.GetAxis(horizontalInputName);

        Vector3 forwardMovement = transform.forward * vertInput;
        Vector3 rightMovement = transform.right * horInput;

        charController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement, 1.0f)
            * movementSpeed);

        if ((vertInput != 0 || horInput != 0) && OnSlope())
              charController.Move(Vector3.down * charController.height / 2 * slopeForce * Time.deltaTime);

        SetMovementSpeed();
        JumpInput();
        CheckIfAiming();
    }

    private void SetMovementSpeed()
    {
        if (Input.GetKey(runKey))
        {
            movementSpeed = Mathf.Lerp(movementSpeed, runSpeed, Time.deltaTime * runBuildUpSpeed);

            if (myCamera.fieldOfView < startFOV + sprintFOVChange)
            {
                myCamera.fieldOfView = Mathf.Lerp(myCamera.fieldOfView, myCamera.fieldOfView + sprintFOVChange, sprintFOVLerpTime);
            }
        }
        else
        {
            movementSpeed = Mathf.Lerp(movementSpeed, walkSpeed, Time.deltaTime * runBuildUpSpeed);

            if (myCamera.fieldOfView > startFOV)
            {
                myCamera.fieldOfView = Mathf.Lerp(myCamera.fieldOfView, myCamera.fieldOfView - sprintFOVChange, sprintFOVLerpTime);
            }
        }
    }

    private bool OnSlope()
    {
        if (isJumping)
            return false;

        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, charController.height / 2 * slopeForceRayLength))
            if (hit.normal != Vector3.up)
                return true;
        return false;
    }


    private void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    private IEnumerator JumpEvent()
    {
        charController.slopeLimit = 90.0f;
        float timeInAir = 0f;

        do
        {
            float jumpForce = jumpFallOff.Evaluate(timeInAir);
            charController.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;
            yield return null;
        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);

        isJumping = false;
        charController.slopeLimit = 45.0f;
    }


    private void CheckIfAiming()
    {
        if (Input.GetButton("Fire2") && !gun.isReloading)
        {
            walkSpeed = Mathf.Lerp(walkSpeed, ADSWalkSlow, slowDownMultiplier * Time.deltaTime);
            runSpeed = Mathf.Lerp(runSpeed, ADSRunSlow, slowDownMultiplier * Time.deltaTime);
        }
        else
        {
            walkSpeed = Mathf.Lerp(walkSpeed, originalWalkSpeed, slowDownMultiplier * Time.deltaTime);
            runSpeed = Mathf.Lerp(runSpeed, originalRunSpeed, slowDownMultiplier * Time.deltaTime);
        }
    }
}